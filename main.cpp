﻿//#include <osg/Shader>
//#include <osg/Node>
//#include <osgDB/ReadFile>
//#include <osgDB/FileUtils>
//#include <osg/Geometry>
//#include <osgViewer/Viewer>

//static std::string vertexShader= {
//    "#version 330 \n"
//    "layout (location=0) in vec3 VertexPosition;\n"
//    "layout (location=1) in vec4 VertexColor;\n"
//    "uniform mat4 MVP;"
//    "out vec4 Color;\n"
//    "void main()\n"
//    "{\n"
//    "   Color = VertexColor;\n"
//    "   gl_Position = MVP * vec4(VertexPosition,1.0);\n"
//    "   }\n"
//};

//static std::string fragShader ={
//    "#version 330 \n"
//    "in vec4 Color;\n"
//    "layout (location=0) out vec4 FragColor;\n"
//    "void main() {\n"
//    "   FragColor = Color;//vec4(0.5,0.5,0.5,0.4);\n"
//    "}\n"
//};


//class MVPCallback: public osg::Uniform::Callback
//{
//public:
//    MVPCallback(osg::Camera * camera):mCamera(camera){
//    }
//    virtual void operator()( osg::Uniform* uniform, osg::NodeVisitor* nv){
//        osg::Matrix modelView = mCamera->getViewMatrix();
//        osg::Matrix projectM = mCamera->getProjectionMatrix();
//        uniform->set(modelView * projectM);
//    }

//private:
//    osg::Camera * mCamera;
//};
//int main(int argc, char *argv[]) {

//    osgViewer::Viewer viewer;

//    osg::Group * root = new osg::Group;
//    osg::ref_ptr<osg::Node>node = CreateNode();

//    osg::StateSet * ss = node->getOrCreateStateSet();
//    osg::Program * program = new osg::Program;
//    program->addBindFragDataLocation("VertexPosition",0);
//    program->addBindFragDataLocation("VertexColor",1);

//    osg::Shader * vS = new osg::Shader(osg::Shader::FRAGMENT,fragShader);
//    osg::Shader * fS = new osg::Shader(osg::Shader::VERTEX,vertexShader);
//    osg::Uniform* MVPUniform = new osg::Uniform( "MVP",osg::Matrix());
//    MVPUniform->setUpdateCallback(new MVPCallback(viewer.getCamera()));
//    ss->addUniform(MVPUniform);//对应的Program和Uniform要加到同一个Node下的StateSet中
//    program->addShader(vS);
//    program->addShader(fS);
//    ss->setAttributeAndModes(program,osg::StateAttribute::ON);

//    root->addChild(node);
//    viewer.setSceneData(root);


//    viewer.run();

//    return 0;
//}


#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/GraphicsContext>
#include <osg/Camera>
#include <osg/Viewport>
#include <osg/StateSet>
#include <osg/Program>
#include <osg/Shader>
#include <osgUtil/Optimizer>



void configureShaders( osg::StateSet* stateSet )
{
    const std::string fragmentSource =
        R"(#version 330
        in vec2 v;
    out vec4 FragColor;
    uniform mat4 ViewMatrix;
    uniform vec2 resolution;
    uniform vec3 lightPos;

    vec3 skyColor( in vec3 rd )
    {
        vec3 sundir = normalize( lightPos );

        float yd = min(rd.y, 0.);
        rd.y = max(rd.y, 0.);

        vec3 col = vec3(0.);

        col += vec3(.9, .9 , .9) * exp(-rd.y*9.); // Red / Green
        col += vec3(.3, .5, .7) * (1. - exp(-rd.y*8.) ) * exp(-rd.y*.9) ; // Blue

        col = mix(col*1.2, vec3(.8),  1.-exp(yd*100.)); // Fog

        col += vec3(1.0, .8, .55) * pow( max(dot(rd,sundir),0.), 15. ) * .3; // Sun
        col += vec3(1.0, .5, .3) * pow(max(dot(rd, sundir),0.), 300.0) ;

        return col;
    }
    void main()
    {
        vec3 dir = vec3( normalize( inverse(ViewMatrix)*vec4(v.xy*vec2(1280/720,1),-1.5,0.0) ) );

        FragColor = vec4(skyColor(dir),1.);
    })";


    const std::string vertexSource =
        "#version 330 \n"
        " \n"
        "in vec3 Position; \n"
        "out vec2 v; \n"
        " \n"
        "void main() \n"
        "{ \n"
        "    v = Position.xy; \n"
        "    gl_Position = vec4(Position, 1.0);\n"
        "} \n";
    osg::Shader* fShader = new osg::Shader( osg::Shader::FRAGMENT, fragmentSource );
    osg::Shader* vShader = new osg::Shader( osg::Shader::VERTEX, vertexSource );
    osg::Program* program = new osg::Program;
    program->addShader( vShader );
    program->addShader( fShader );
    stateSet->setAttribute( program );

    osg::Vec3f lightDir( 0., 0.5, 1. );
    lightDir.normalize();
    stateSet->addUniform( new osg::Uniform( "lightPos", lightDir ) );
    stateSet->addUniform( new osg::Uniform("ViewMatrix", osg::Matrixd()));
}





//int main( int argc, char** argv )
//{
//    osg::ArgumentParser arguments( &argc, argv );

//    osg::ref_ptr<osg::Node> root = osgDB::readRefNodeFiles( arguments );
//    if( root == NULL )
//    {
//        osg::notify( osg::FATAL ) << "Unable to load model from command line." << std::endl;
//        return( 1 );
//    }

//    osgUtil::Optimizer optimizer;
//    optimizer.optimize(root.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS  | osgUtil::Optimizer::TESSELLATE_GEOMETRY);

//    configureShaders( root->getOrCreateStateSet() );

//    const int width( 800 ), height( 450 );
//    const std::string version( "3.3" );
//    osg::ref_ptr< osg::GraphicsContext::Traits > traits = new osg::GraphicsContext::Traits();
//    traits->x = 20; traits->y = 30;
//    traits->width = width; traits->height = height;
//    traits->windowDecoration = true;
//    traits->doubleBuffer = true;
//    traits->glContextVersion = version;
//    traits->readDISPLAY();
//    traits->setUndefinedScreenDetailsToDefaultScreen();
//    osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext( traits.get() );
//    if( !gc.valid() )
//    {
//        osg::notify( osg::FATAL ) << "Unable to create OpenGL v" << version << " context." << std::endl;
//        return( 1 );
//    }

//    osgViewer::Viewer viewer;

//    // Create a Camera that uses the above OpenGL context.
//    osg::Camera* cam = viewer.getCamera();
//    cam->setGraphicsContext( gc.get() );
//    // Must set perspective projection for fovy and aspect.
//    cam->setProjectionMatrix( osg::Matrix::perspective( 30., (double)width/(double)height, 1., 100. ) );
//    // Unlike OpenGL, OSG viewport does *not* default to window dimensions.
//    cam->setViewport( new osg::Viewport( 0, 0, width, height ) );

//    viewer.setSceneData( root );

//    // for non GL3/GL4 and non GLES2 platforms we need enable the osg_ uniforms that the shaders will use,
//    // you don't need thse two lines on GL3/GL4 and GLES2 specific builds as these will be enable by default.
//    gc->getState()->setUseModelViewAndProjectionUniforms(true);
//    gc->getState()->setUseVertexAttributeAliasing(true);

//    return( viewer.run() );
//}


#include <stdlib.h>
#include <stdio.h>
#include <iostream>

#include <QApplication>
#include <QMainWindow>
#include <QOpenGLWidget>
#include <QMouseEvent>
#include <QWheelEvent>
#include <QDesktopWidget>
#include <QScreen>
#include <QtGlobal>
#include <QWindow>

#include <osg/ref_ptr>
#include <osgViewer/GraphicsWindow>
#include <osgViewer/Viewer>
#include <osg/Camera>
#include <osg/ShapeDrawable>
#include <osg/StateSet>
#include <osg/Material>
#include <osgGA/EventQueue>
#include <osgGA/TrackballManipulator>

//osg::Node *  CreateNode()
//{
//    osg::Geode * geode = new osg::Geode;
//    osg::Geometry* polyGeom = new osg::Geometry();
//    osg::Vec3Array* vertices = new osg::Vec3Array();
//    vertices->push_back(osg::Vec3(-5,0,0));
//    vertices->push_back(osg::Vec3(5,0,0));
//    vertices->push_back(osg::Vec3(0,0,5));
//    polyGeom->setVertexArray(vertices);


//    osg::ref_ptr<osg::Vec4Array> colorsArray = new osg::Vec4Array;
//    colorsArray->push_back(osg::Vec4(1.0f,0.0f,0.0f,1.0f));
//    colorsArray->push_back(osg::Vec4(0.0f,0.0f,1.0f,1.0f));
//    colorsArray->push_back(osg::Vec4(0.0f,1.0f,0.0f,1.0f));
//    polyGeom->setColorArray(colorsArray.get());
//    polyGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

//    polyGeom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES,0,3));

//    /*
//        The osg::Geometry class uses the setVertexAttribArray() and
//        setVertexAttribBinding() methods to bind vertex attributes to shaders. They should
//        be provided per vertex. GLSL's built-in vertex attributes include the gl_Position, gl_
//        Normal, and gl_MultiTexCoord* variables. However, you may still specify your own
//        vertex attributes, such as tangents or vertex weights.
//        Try declaring an attribute in the vertex shader and make use of the osg::Geometry's vertex
//        attribute arrays. Another important task that you need to perform is to bind the external
//        attribute array and the GLSL attribute, with the help of the addBindAttribLocation()
//        method of osg::Program. It has a name and an index parameter, the first of which
//        indicates the attribute name in the shader source code, and the second should correspond
//        to the input index value of setVertexAttribArray().
//        */
//    polyGeom->setVertexAttribArray(0,vertices);
//    polyGeom->setVertexAttribBinding(0, osg::Geometry::BIND_PER_VERTEX);
//    polyGeom->setVertexAttribArray(1,colorsArray.get());
//    polyGeom->setVertexAttribBinding(1, osg::Geometry::BIND_PER_VERTEX);

//    geode->addDrawable(polyGeom);
//    return geode;
//}

class QtOSGWidget : public QOpenGLWidget
{
public:
    QtOSGWidget(QWidget* parent = 0)
        : QOpenGLWidget(parent)
        , _mGraphicsWindow(new osgViewer::GraphicsWindowEmbedded( this->x(), this->y(),
              this->width(), this->height() ) )
        , _mViewer(new osgViewer::Viewer)
        // take care of HDPI screen, e.g. Retina display on Mac
        , m_scale(QApplication::desktop()->devicePixelRatio())
    {
        osg::Box* cylinder    = new osg::Box( osg::Vec3( 0.f, 0.f, 0.f ), 1.f);
        osg::ShapeDrawable* sd = new osg::ShapeDrawable( cylinder );
        sd->setColor( osg::Vec4( 0.8f, 0.5f, 0.2f, 1.f ) );
        osg::Geode* geode = new osg::Geode;
        geode->addDrawable(sd);


        osg::Cylinder* cylinder1    = new osg::Cylinder( osg::Vec3( 0.f, 0.f, 0.f ), 0.5f, 0.5f );
        osg::ShapeDrawable* sd1 = new osg::ShapeDrawable( cylinder1 );
        sd1->setColor( osg::Vec4( 0.8f, 0.5f, 0.2f, 1.f ) );

        osg::Geode* geode1 = new osg::Geode;
        geode1->addDrawable(sd);

        geode->addChild(geode1);

        osg::Camera* camera = new osg::Camera;

        camera->setViewport( 0, 0, this->width(), this->height() );
        camera->setClearColor( osg::Vec4( 0.9f, 0.9f, 1.f, 1.f ) );
        float aspectRatio = static_cast<float>( this->width()) / static_cast<float>( this->height() );
        camera->setProjectionMatrixAsPerspective( 30.f, aspectRatio, 1.f, 1000.f );
        camera->setGraphicsContext( _mGraphicsWindow );
        camera->getClearDepth();

        _mViewer->setCamera(camera);
        _mViewer->setSceneData(geode);
        configureShaders(geode1->getOrCreateStateSet());
        osgGA::TrackballManipulator* manipulator = new osgGA::TrackballManipulator;
        manipulator->setAllowThrow( false );
        this->setMouseTracking(true);
        _mViewer->setCameraManipulator(manipulator);
        _mViewer->setThreadingModel(osgViewer::Viewer::SingleThreaded);
        _mViewer->realize();
    }


    virtual ~QtOSGWidget(){}

protected:

    virtual void paintGL() {
        _mViewer->frame();
        osg::Camera* camera = _mViewer->getCamera();
        osg::Geode* geode = dynamic_cast<osg::Geode*>(_mViewer->getSceneData());
        auto* ge = geode->getChild(0);
        osg::Uniform* uniform = ge->getOrCreateStateSet()->getOrCreateUniform("ViewMatrix",osg::Uniform::FLOAT_MAT4);
        uniform->set(camera->getViewMatrix());
    }

    virtual void resizeGL( int width, int height )
    {
        this->getEventQueue()->windowResize(this->x()*m_scale, this->y() * m_scale, width*m_scale, height*m_scale);
        _mGraphicsWindow->resized(this->x()*m_scale, this->y() * m_scale, width*m_scale, height*m_scale);
        osg::Camera* camera = _mViewer->getCamera();
        camera->setViewport(0, 0, this->width()*m_scale, this->height()* m_scale);
    }

    virtual void initializeGL(){
        osg::Geode* geode = dynamic_cast<osg::Geode*>(_mViewer->getSceneData());
        osg::StateSet* stateSet = geode->getOrCreateStateSet();
        osg::Material* material = new osg::Material;
        material->setColorMode( osg::Material::AMBIENT_AND_DIFFUSE );
        stateSet->setAttributeAndModes( material, osg::StateAttribute::ON );
        stateSet->setMode( GL_DEPTH_TEST, osg::StateAttribute::ON );
    }

    virtual void mouseMoveEvent(QMouseEvent* event)
    {
        this->getEventQueue()->mouseMotion(event->x()*m_scale, event->y()*m_scale);
    }

    virtual void mousePressEvent(QMouseEvent* event)
    {
        unsigned int button = 0;
        switch (event->button()){
        case Qt::LeftButton:
            button = 1;
            break;
        case Qt::MiddleButton:
            button = 2;
            break;
        case Qt::RightButton:
            button = 3;
            break;
        default:
            break;
        }
        this->getEventQueue()->mouseButtonPress(event->x()*m_scale, event->y()*m_scale, button);
    }

    virtual void mouseReleaseEvent(QMouseEvent* event)
    {
        unsigned int button = 0;
        switch (event->button()){
        case Qt::LeftButton:
            button = 1;
            break;
        case Qt::MiddleButton:
            button = 2;
            break;
        case Qt::RightButton:
            button = 3;
            break;
        default:
            break;
        }
        this->getEventQueue()->mouseButtonRelease(event->x()*m_scale, event->y()*m_scale, button);
    }

    virtual void wheelEvent(QWheelEvent* event)
    {
        int delta = event->delta();
        osgGA::GUIEventAdapter::ScrollingMotion motion = delta > 0 ?
            osgGA::GUIEventAdapter::SCROLL_UP : osgGA::GUIEventAdapter::SCROLL_DOWN;
        this->getEventQueue()->mouseScroll(motion);
    }

    virtual bool event(QEvent* event)
    {
        bool handled = QOpenGLWidget::event(event);
        this->update();
        return handled;
    }

private:

    osgGA::EventQueue* getEventQueue() const {
        osgGA::EventQueue* eventQueue = _mGraphicsWindow->getEventQueue();
        return eventQueue;
    }

    osg::ref_ptr<osgViewer::GraphicsWindowEmbedded> _mGraphicsWindow;
    osg::ref_ptr<osgViewer::Viewer> _mViewer;
    qreal m_scale;
};

int main(int argc, char** argv)
{
#if QT_VERSION >= QT_VERSION_CHECK(5,6,0)
    QApplication::setAttribute(Qt::AA_DisableHighDpiScaling);
#else
    qputenv("QT_DEVICE_PIXEL_RATIO", QByteArray("1"));
#endif

    QApplication qapp(argc, argv);
    QMainWindow window;
    QtOSGWidget* widget = new QtOSGWidget(&window);
    window.setCentralWidget(widget);
    window.show();

    return qapp.exec();
}
