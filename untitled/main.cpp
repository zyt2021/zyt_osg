﻿//#include <osg/Shader>
//#include <osg/Node>
//#include <osgDB/ReadFile>
//#include <osgDB/FileUtils>
//#include <osg/Geometry>
//#include <osgViewer/Viewer>

//static std::string vertexShader= {
//    "#version 330 \n"
//    "layout (location=0) in vec3 VertexPosition;\n"
//    "layout (location=1) in vec4 VertexColor;\n"
//    "uniform mat4 MVP;"
//    "out vec4 Color;\n"
//    "void main()\n"
//    "{\n"
//    "   Color = VertexColor;\n"
//    "   gl_Position = MVP * vec4(VertexPosition,1.0);\n"
//    "   }\n"
//};

//static std::string fragShader ={
//    "#version 330 \n"
//    "in vec4 Color;\n"
//    "layout (location=0) out vec4 FragColor;\n"
//    "void main() {\n"
//    "   FragColor = Color;//vec4(0.5,0.5,0.5,0.4);\n"
//    "}\n"
//};
//osg::Node *  CreateNode()
//{
//    osg::Geode * geode = new osg::Geode;
//    osg::Geometry* polyGeom = new osg::Geometry();
//    osg::Vec3Array* vertices = new osg::Vec3Array();
//    vertices->push_back(osg::Vec3(-5,0,0));
//    vertices->push_back(osg::Vec3(5,0,0));
//    vertices->push_back(osg::Vec3(0,0,5));
//    polyGeom->setVertexArray(vertices);


//    osg::ref_ptr<osg::Vec4Array> colorsArray = new osg::Vec4Array;
//    colorsArray->push_back(osg::Vec4(1.0f,0.0f,0.0f,1.0f));
//    colorsArray->push_back(osg::Vec4(0.0f,0.0f,1.0f,1.0f));
//    colorsArray->push_back(osg::Vec4(0.0f,1.0f,0.0f,1.0f));
//    polyGeom->setColorArray(colorsArray.get());
//    polyGeom->setColorBinding(osg::Geometry::BIND_PER_VERTEX);

//    polyGeom->addPrimitiveSet(new osg::DrawArrays(osg::PrimitiveSet::TRIANGLES,0,3));

//    /*
//        The osg::Geometry class uses the setVertexAttribArray() and
//        setVertexAttribBinding() methods to bind vertex attributes to shaders. They should
//        be provided per vertex. GLSL's built-in vertex attributes include the gl_Position, gl_
//        Normal, and gl_MultiTexCoord* variables. However, you may still specify your own
//        vertex attributes, such as tangents or vertex weights.
//        Try declaring an attribute in the vertex shader and make use of the osg::Geometry's vertex
//        attribute arrays. Another important task that you need to perform is to bind the external
//        attribute array and the GLSL attribute, with the help of the addBindAttribLocation()
//        method of osg::Program. It has a name and an index parameter, the first of which
//        indicates the attribute name in the shader source code, and the second should correspond
//        to the input index value of setVertexAttribArray().
//        */
//    polyGeom->setVertexAttribArray(0,vertices);
//    polyGeom->setVertexAttribBinding(0, osg::Geometry::BIND_PER_VERTEX);
//    polyGeom->setVertexAttribArray(1,colorsArray.get());
//    polyGeom->setVertexAttribBinding(1, osg::Geometry::BIND_PER_VERTEX);

//    geode->addDrawable(polyGeom);
//    return geode;
//}

//class MVPCallback: public osg::Uniform::Callback
//{
//public:
//    MVPCallback(osg::Camera * camera):mCamera(camera){
//    }
//    virtual void operator()( osg::Uniform* uniform, osg::NodeVisitor* nv){
//        osg::Matrix modelView = mCamera->getViewMatrix();
//        osg::Matrix projectM = mCamera->getProjectionMatrix();
//        uniform->set(modelView * projectM);
//    }

//private:
//    osg::Camera * mCamera;
//};
//int main(int argc, char *argv[]) {

//    osgViewer::Viewer viewer;

//    osg::Group * root = new osg::Group;
//    osg::ref_ptr<osg::Node>node = CreateNode();

//    osg::StateSet * ss = node->getOrCreateStateSet();
//    osg::Program * program = new osg::Program;
//    program->addBindFragDataLocation("VertexPosition",0);
//    program->addBindFragDataLocation("VertexColor",1);

//    osg::Shader * vS = new osg::Shader(osg::Shader::FRAGMENT,fragShader);
//    osg::Shader * fS = new osg::Shader(osg::Shader::VERTEX,vertexShader);
//    osg::Uniform* MVPUniform = new osg::Uniform( "MVP",osg::Matrix());
//    MVPUniform->setUpdateCallback(new MVPCallback(viewer.getCamera()));
//    ss->addUniform(MVPUniform);//对应的Program和Uniform要加到同一个Node下的StateSet中
//    program->addShader(vS);
//    program->addShader(fS);
//    ss->setAttributeAndModes(program,osg::StateAttribute::ON);

//    root->addChild(node);
//    viewer.setSceneData(root);


//    viewer.run();

//    return 0;
//}


#include <osgViewer/Viewer>
#include <osgDB/ReadFile>
#include <osg/GraphicsContext>
#include <osg/Camera>
#include <osg/Viewport>
#include <osg/StateSet>
#include <osg/Program>
#include <osg/Shader>
#include <osgUtil/Optimizer>

void configureShaders( osg::StateSet* stateSet )
{
    const std::string vertexSource =
        "#version 330 \n"
        " \n"
        "uniform mat4 osg_ModelViewProjectionMatrix; \n"
        "uniform mat3 osg_NormalMatrix; \n"
        "uniform vec3 ecLightDir; \n"
        " \n"
        "in vec4 osg_Vertex; \n"
        "in vec3 osg_Normal; \n"
        "out vec4 color; \n"
        " \n"
        "void main() \n"
        "{ \n"
        "    vec3 ecNormal = normalize( osg_NormalMatrix * osg_Normal ); \n"
        "    float diffuse = max( dot( ecLightDir, ecNormal ), 0. ); \n"
        "    color = vec4( vec3( diffuse ), 1. ); \n"
        " \n"
        "    gl_Position = osg_ModelViewProjectionMatrix * osg_Vertex; \n"
        "} \n";
    osg::Shader* vShader = new osg::Shader( osg::Shader::VERTEX, vertexSource );

    const std::string fragmentSource =
        "#version 330 \n"
        " \n"
        "in vec4 color; \n"
        "out vec4 fragData; \n"
        " \n"
        "void main() \n"
        "{ \n"
        "    fragData = color; \n"
        "} \n";
    osg::Shader* fShader = new osg::Shader( osg::Shader::FRAGMENT, fragmentSource );

    osg::Program* program = new osg::Program;
    program->addShader( vShader );
    program->addShader( fShader );
    stateSet->setAttribute( program );

    osg::Vec3f lightDir( 0., 0.5, 1. );
    lightDir.normalize();
    stateSet->addUniform( new osg::Uniform( "ecLightDir", lightDir ) );
}

int main( int argc, char** argv )
{
    osg::ArgumentParser arguments( &argc, argv );

    osg::ref_ptr<osg::Node> root = osgDB::readRefNodeFiles( arguments );
    if( root == NULL )
    {
        osg::notify( osg::FATAL ) << "Unable to load model from command line." << std::endl;
        return( 1 );
    }

    osgUtil::Optimizer optimizer;
    optimizer.optimize(root.get(), osgUtil::Optimizer::ALL_OPTIMIZATIONS  | osgUtil::Optimizer::TESSELLATE_GEOMETRY);

    configureShaders( root->getOrCreateStateSet() );

    const int width( 800 ), height( 450 );
    const std::string version( "3.3" );
    osg::ref_ptr< osg::GraphicsContext::Traits > traits = new osg::GraphicsContext::Traits();
    traits->x = 20; traits->y = 30;
    traits->width = width; traits->height = height;
    traits->windowDecoration = true;
    traits->doubleBuffer = true;
    traits->glContextVersion = version;
    traits->readDISPLAY();
    traits->setUndefinedScreenDetailsToDefaultScreen();
    osg::ref_ptr< osg::GraphicsContext > gc = osg::GraphicsContext::createGraphicsContext( traits.get() );
    if( !gc.valid() )
    {
        osg::notify( osg::FATAL ) << "Unable to create OpenGL v" << version << " context." << std::endl;
        return( 1 );
    }

    osgViewer::Viewer viewer;

    // Create a Camera that uses the above OpenGL context.
    osg::Camera* cam = viewer.getCamera();
    cam->setGraphicsContext( gc.get() );
    // Must set perspective projection for fovy and aspect.
    cam->setProjectionMatrix( osg::Matrix::perspective( 30., (double)width/(double)height, 1., 100. ) );
    // Unlike OpenGL, OSG viewport does *not* default to window dimensions.
    cam->setViewport( new osg::Viewport( 0, 0, width, height ) );

    viewer.setSceneData( root );

    // for non GL3/GL4 and non GLES2 platforms we need enable the osg_ uniforms that the shaders will use,
    // you don't need thse two lines on GL3/GL4 and GLES2 specific builds as these will be enable by default.
    gc->getState()->setUseModelViewAndProjectionUniforms(true);
    gc->getState()->setUseVertexAttributeAliasing(true);

    return( viewer.run() );
}
